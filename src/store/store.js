import Vue from 'vue'
import vuex from 'vuex'
Vue.use(vuex)

export default new vuex.Store({
	state:{
		price:"",    //单个商品的价格详情
		number:1,    //商品的购买件数
		size:['S'],     //商品的尺寸    是个数组
		information:'',  //获取客户信息
		share_imgSrc:{ //分享图得地址
			boyImg:'',
			girlImg:'',
		}, 
		type:'zh',        //获取design小图的类型  有zh   en   img
		is_action:"1",    //判断是否在活动期内
		picture_src:{       //获取design页面的正反面图案
			material_up_src:"",
			material_up_id:"",
			material_down_src:"",
			material_down_id:""
		},   
		img_src:{           //design-footer点击后图片展示图
			img_src:"",
			img_id:""
		},
		face:"",            //设置当前衣服是正面还是反面
		errorShow:{         //用于判断是否填满信息
			nameError:false,
	        areaError:false,
	        telError:false
		},
		sexImg:"1",        //获取用户分享的是男女模特的照片 
		priceShow:true,     //获取价格显示框的状态      

	},
	mutations:{
		getPrice(state,value){        //获取单个商品的价格
           state.price=value;
           window.localStorage.setItem('price', JSON.stringify(state.price));    //因为页面刷新会导致store的值消失，所以要保存到本地
		},
		getNumber(state,value){      //获取购买商品的数量
			state.number=value;
		},
		getSize(state,value){      //获取购买商品的数量
			state.size=value;
		},
		setInformation(state,value)
		{
		    state.information=value;    //获取客户信息
		},
		setShareImg(state,value)//获取分享图的src
		{
		    state.share_imgSrc.boyImg=value.boy.img_url;    
		    state.share_imgSrc.girlImg=value.girl.img_url;   
		},
		setSmallImg(state,value)
		{
		    state.type=value;    //获取分享图的src
		},
		setIsActive(state,value)
		{
		    state.is_action=value;    //获取分享图的src
		    window.localStorage.setItem('is_action', JSON.stringify(state.is_action));
		},
        setImg(state,value)       //获取design当前衣服设计的图案  用来生成合成图
		{
	        if(state.face==='0')
	        {
	          state.picture_src.material_up_src=value[1];
	          state.picture_src.material_up_id=value[0];
	        }
	        else
	        {
	          state.picture_src.material_down_src=value[1];
	          state.picture_src.material_down_id=value[0]
	        }
		},
		setFace(state,value)
		{
		    state.face=value;   //设置当前衣服是正面还是反面
		},
		setErrorShow(state,value)
		{
		    state.errorShow.nameError=value.nameError;   //设置当前的nameinput是否填写满
		    state.errorShow.telError=value.telError;   //设置当前的nameinput是否填写满
		    state.errorShow.areaError=value.areaError;   //设置当前的area和cityinput是否填写满
		},
		setSexImg(state,value)
		{
		    state.sexImg=value;   //设置当前模特是男还是女
		},
		setPriceShow(state,value)
		{
		    state.priceShow=value;   //设置当前模特是男还是女
		},
	},
	actions:{
		getPrice(context,price)
		{
         context.commit('getPrice',price)      //获取单个商品的价格
		},
		getNumber(context,number)
		{
		 context.commit('getNumber',number)     //获取购买商品的数量
		},
		getSize(context,size)
		{
		 context.commit('getSize',size)     //获取购买商品的数量
		},
		setInformation(context,information)
		{
		 context.commit('setInformation',information)     //获取客户信息
		},
		setShareImg(context,share_imgSrc)
		{
		    context.commit('setShareImg',share_imgSrc)    //获取分享图的src
		},
		setSmallImg(context,small_imgType)
		{
		    context.commit('setSmallImg',small_imgType)    //获取分享图的src
		},
		setIsActive(context,is_action)
		{
		    context.commit('setIsActive',is_action)    //获取活动时间
		},
		setImg(context,imgSrc)
		{
		    context.commit('setImg',imgSrc)    //获取desgin-footer页面的点击事件传的数据
		},
		setFace(context,value)
		{
		    context.commit('setFace',value)    //获取design页面传过来的face值
		},
		setErrorShow(context,value)
		{
		    context.commit('setErrorShow',value)    //获取design页面传过来的face值
		},
		setSexImg(context,value)
		{
		    context.commit('setSexImg',value)    //获取当前模特照片的性别
		},
		setPriceShow(context,value)
		{
		    context.commit('setPriceShow',value)    //获取当前模特照片的性别
		},
	}
})