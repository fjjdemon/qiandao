// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'lib-flexible'
import axios from './utils/axios'
import store from './store/store'
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad,{
  error:'./static/img/loading.svg',
  loading:'./static/img/loading.svg'
})
// 
// 
// 
import clipboard from 'clipboard';
import currency from './utils/index'
import Vant from 'vant';
import 'vant/lib/index.css';
import  './utils/interceptor';

Vue.use(Vant);
axios.defaults.withCredentials=true
Vue.prototype.$axios=axios 
Vue.prototype.$currency=currency 
Vue.prototype.$bus=new Vue() 
Vue.prototype.$clipboard = clipboard;
Vue.config.productionTip = false
// Vue.prototype.bus = new Vue;
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
