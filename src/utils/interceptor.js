/**
 * Created by AlickChen on 2018/11/9.
 */
import axios from './axios'
import router from '../router/index'
import {showToast} from './index.js'
let myInterceptor= axios.interceptors.response.use(function (res) {
        if( res.data.data === 'no_login'&& router.currentRoute.name!='index' && router.currentRoute.name!='peopleSign') {
            showToast('请先授权',1500,'fail');
                setTimeout(()=>{
                   window.location.href="http://zddxapi.rzkeji.com/api/auth/wxLogin";
                },1500)
          }
    
  
  return res
});

export function removeMyInterceptor(){//移除拦截器
    axios.interceptors.response.eject(myInterceptor)
}
 export function is_weixn(){
    var ua = navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i)=="micromessenger") {
        return true;
    } else {
        return false;
    }
}


//  export function openInWebview () {
//     var ua = navigator.userAgent.toLowerCase()
//     if (ua.match(/MicroMessenger/i) == 'micromessenger') { // 微信浏览器判断
//         return false
//     } else if (ua.match(/QQ/i) == 'qq') { // QQ浏览器判断
//         return false
//     } else if (ua.match(/WeiBo/i) == "weibo") {
//         return false
//     } else {
//         if (ua.match(/Android/i) != null) {
//             return ua.match(/browser/i) == null
//         } else if (ua.match(/iPhone/i) != null) {
//             return ua.match(/safari/i) == null
//         } else {
//             return (ua.match(/macintosh/i) == null && ua.match(/windows/i) == null)
//         }
//     }
// }

