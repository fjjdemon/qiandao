import { Toast,Dialog  } from 'vant';



export function sexTag(value){
  if(value=="男"){
    return 1
  }else if(value=="女"){
    return 2
  }else{
    return 3
  }
}

export function sexTag1(value){
  if(value==1){
    return '男'
  }else if(value==2){
    return '女'
  }else{
    return '保密'
  }
}

export function showToast(message='修改成功45646',time=1500,show='success'){
  if(show=='success'){
    Toast.success({
      message: message,
      duration:time
    })
  }else{
    Toast.fail({
      message: message,
      duration:time
    })
  }
}

// export function showModal(title,content){
//   var _this=this
//   let promise=new Promise((resolve ,reject) => {
//     wx.showModal({
//       title: title,
//       content: content,
//       success (res) {
//         if (res.confirm) {
//           resolve(true)
//         } else if (res.cancel) {
//           resolve(false)
//         }
//       },
//       fail:err=>{
//         reject(err)
//       }
//     })
//   })
//   return promise
// }

export function showModal(title,content){
    return Dialog.confirm({
      title: title,
      message: content
    }).then(() => {
      return true
    }).catch(()=>{
      return false
    })
    
}

export function spliceData(data){//截取日期
  let pattern=/\d*-\d*-\d*/i
  return pattern.exec(data)[0]
}



export function upLoadImage(url){
  let promise=new Promise((resolve,reject)=>{
    axios.post({
      url:"https://wx.zhaowea.com/api/file/upload-image"
    })
    
  })
}
export function trim(str){//去除前后空格
  let strn=str.replace(/(^\s*)|(\s*$)/g,'')
  return strn
}

export default {

  sexTag,
  sexTag1,
  showModal,
  trim,
  spliceData
}


