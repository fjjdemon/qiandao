import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'//首页
import information from '@/components/information'//填写个人资料
import informationSign from '@/components/informationSign'//签到资料填写
import generate from '@/components/generate'//签到网址生成
import meCheck from '@/components/meCheck'   //我发起的签到
import attendanceList from '@/components/attendanceList'   //签到名单
import codeSign from '@/components/codeSign'   //二维码签到
import peopleSign from '@/components/peopleSign'   //二维码签到


Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/information',
      name: 'information',
      component: information
    },
    {
      path: '/informationSign',
      name: 'informationSign',
      component: informationSign
    },
    {
      path: '/generate',
      name: 'generate',
      component: generate
    },
	{
      path: '/meCheck',
      name: 'meCheck',
      component: meCheck
    },
    {
      path: '/attendanceList',
      name: 'attendanceList',
      component: attendanceList
    },
    {
      path: '/codeSign',
      name: 'codeSign',
      component: codeSign
    },
    {
      path: '/peopleSign',
      name: 'peopleSign',
      component: peopleSign
    }
  ]
})
